function initShareUrlPlugin (root) {
  'use strict'

  // imports
  // var cordova = require('cordova')

  var PLUGIN_NAME = 'ShareUrlPlugin'

  // the returned object
  var shareurl = {}

  //
  // exported constants
  //

  // logging levels
  var DEBUG = shareurl.DEBUG = 0
  var INFO = shareurl.INFO = 10
  var WARN = shareurl.WARN = 20
  var ERROR = shareurl.ERROR = 30

  // actions
  shareurl.SEND = 'SEND'
  shareurl.VIEW = 'VIEW'

  //
  // state variables
  //

  // default verbosity level is to show errors only
  var verbosity

  // list of registered handlers
  var handlers

  // list of intents sent to this app
  //
  // it's never cleaned up, so that newly registered handlers (especially those registered a bit too late)
  // will still receive the list of intents.
  var intents

  // the logger function (defaults to console.log)
  var logger

  // the cordova object (defaults to global one)
  var cordova

  // has init() been called or not already
  var initCalled

  // make sure a number is displayed with 2 digits
  var twoDigits = function (n) {
    return n < 10
      ? '0' + n
      : '' + n
  }

  // format a date for display
  var formatDate = function (now) {
    var date = now ? new Date(now) : new Date()
    var d = [date.getMonth() + 1, date.getDate()].map(twoDigits)
    var t = [date.getHours(), date.getMinutes(), date.getSeconds()].map(twoDigits)
    return d.join('-') + ' ' + t.join(':')
  }

  // format verbosity level for display
  var formatVerbosity = function (level) {
    if (level <= DEBUG) return 'D'
    if (level <= INFO) return 'I'
    if (level <= WARN) return 'W'
    return 'E'
  }

  // display a log in the console only if the level is higher than current verbosity
  var log = function (level, message) {
    if (level >= verbosity) {
      logger(formatDate() + ' ' + formatVerbosity(level) + ' shareurl: ' + message)
    }
  }

  // reset the state to default
  shareurl.reset = function () {
    log(DEBUG, 'reset')
    verbosity = shareurl.INFO
    handlers = []
    intents = []
    logger = console.log
    cordova = root.cordova
    initCalled = false
  }

  // perform the initial reset
  shareurl.reset()

  // change the logger function
  shareurl.setLogger = function (value) {
    logger = value
  }

  // change the cordova object (mostly for testing)
  shareurl.setCordova = function (value) {
    cordova = value
  }

  // change the verbosity level
  shareurl.setVerbosity = function (value) {
    log(DEBUG, 'setVerbosity()')
    if (value !== DEBUG && value !== INFO && value !== WARN && value !== ERROR) {
      throw new Error('invalid verbosity level')
    }
    verbosity = value
    cordova.exec(null, null, PLUGIN_NAME, 'setVerbosity', [value])
  }

  // retrieve the verbosity level
  shareurl.getVerbosity = function () {
    log(DEBUG, 'getVerbosity()')
    return verbosity
  }

  // a simple function to test that the plugin is correctly installed
  shareurl.about = function () {
    log(DEBUG, 'about()')
    return 'cordova-plugin-shareurl, (c) 2019 insights.md'
  }

  var findHandler = function (callback) {
    for (var i = 0; i < handlers.length; ++i) {
      if (handlers[i] === callback) {
        return i
      }
    }
    return -1
  }

  // registers a intent handler
  shareurl.addHandler = function (callback) {
    log(DEBUG, 'addHandler()')
    if (typeof callback !== 'function') {
      throw new Error('invalid handler function')
    }
    if (findHandler(callback) >= 0) {
      throw new Error('handler already defined')
    }
    handlers.push(callback)
    intents.forEach(function handleIntent (intent) {
      callback(intent)
    })
  }

  shareurl.numHandlers = function () {
    log(DEBUG, 'numHandler()')
    return handlers.length
  }

  shareurl.load = function (dataDescriptor, successCallback, errorCallback) {
    var loadSuccess = function (base64) {
      dataDescriptor.base64 = base64
      if (successCallback) {
        successCallback(base64, dataDescriptor)
      }
    }
    var loadError = function (err) {
      if (errorCallback) {
        errorCallback(err, dataDescriptor)
      }
    }
    if (dataDescriptor.base64) {
      loadSuccess(dataDescriptor.base64)
    } else {
      cordova.exec(loadSuccess, loadError, PLUGIN_NAME, 'load', [dataDescriptor])
    }
  }

  shareurl.exit = function () {
    log(DEBUG, 'exit()')
    cordova.exec(null, null, PLUGIN_NAME, 'exit', [])
  }

  var onNewIntent = function (intent) {
    log(DEBUG, 'onNewIntent(' + intent.action + ')')
    // process the new intent
    handlers.forEach(function (handler) {
      handler(intent)
    })
    intents.push(intent)
  }

  // Initialize the native side at startup
  shareurl.init = function (successCallback, errorCallback) {
    log(DEBUG, 'init()')
    if (initCalled) {
      throw new Error('init should only be called once')
    }
    initCalled = true

    // callbacks have to be functions
    if (successCallback && typeof successCallback !== 'function') {
      throw new Error('invalid success callback')
    }
    if (errorCallback && typeof errorCallback !== 'function') {
      throw new Error('invalid error callback')
    }

    var initSuccess = function () {
      log(DEBUG, 'initSuccess()')
      if (successCallback) successCallback()
    }
    var initError = function () {
      log(DEBUG, 'initError()')
      if (errorCallback) errorCallback()
    }
    var nativeLogger = function (data) {
      var split = data.split(':')
      log(+split[0], '[native] ' + split.slice(1).join(':'))
    }

    cordova.exec(nativeLogger, null, PLUGIN_NAME, 'setLogger', [])
    cordova.exec(onNewIntent, null, PLUGIN_NAME, 'setHandler', [])
    cordova.exec(initSuccess, initError, PLUGIN_NAME, 'init', [])
  }

  return shareurl
}

// Export the plugin object
var shareurl = initShareUrlPlugin(this)
module.exports = shareurl
this.plugins = this.plugins || {}
this.plugins.shareurl = shareurl
