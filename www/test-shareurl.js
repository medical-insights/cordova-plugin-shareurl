/* global describe it beforeEach */
var shareurl = require('./shareurl')
var expect = require('expect.js')

describe('shareurl', () => {
  var cordovaExecArgs

  var cordovaExecCallTo = function (method) {
    return cordovaExecArgs[method]
  }

  // cordova fail-over (to run tests)
  var fakeCordova = () => {
    return {
      exec: function (successCallback, errorCallback, targetObject, method, args) {
        cordovaExecArgs[method] = {
          successCallback: successCallback,
          errorCallback: errorCallback,
          targetObject: targetObject,
          method: method,
          args: args
        }
      }
    }
  }

  beforeEach(() => {
    // start each test from a clean state
    shareurl.reset()

    // setup a fake cordova engine
    shareurl.setCordova(fakeCordova())

    // disable all logging
    shareurl.setLogger(() => {})
    // shareurl.setVerbosity(0)

    cordovaExecArgs = {}
  })

  describe('.about()', () => {
    it('is a function', () => expect(shareurl.about).to.be.a('function'))
    it('returns a string', () => expect(shareurl.about()).to.be.a('string'))
  })

  describe('.init()', () => {
    it('is a function', () => expect(shareurl.init).to.be.a('function'))
    it('can only be called once', () => {
      expect(shareurl.init).withArgs(null, null).to.not.throwError()
      expect(shareurl.init).withArgs(null, null).to.throwError()
    })
    it('accepts a success and error callback', () => {
      var success = () => {}
      var error = () => {}
      expect(shareurl.init).withArgs(success, error).to.not.throwError()
    })
    it('rejects bad argument types', () => {
      var cb = () => {}
      expect(shareurl.init).withArgs(cb, 1).to.throwError()
      expect(shareurl.init).withArgs(1, cb).to.throwError()
    })
  })

  describe('.getVerbosity()', () => {
    it('is a function', () => expect(shareurl.getVerbosity).to.be.a('function'))
    it('returns the verbosity level', () => expect(shareurl.getVerbosity()).to.be.a('number'))
  })

  describe('.setVerbosity()', () => {
    it('is a function', () => expect(shareurl.setVerbosity).to.be.a('function'))
    it('accepts only valid verbosity levels', () => {
      expect(shareurl.setVerbosity).withArgs(shareurl.DEBUG).to.not.throwError()
      expect(shareurl.setVerbosity).withArgs(shareurl.INFO).to.not.throwError()
      expect(shareurl.setVerbosity).withArgs(shareurl.WARN).to.not.throwError()
      expect(shareurl.setVerbosity).withArgs(shareurl.ERROR).to.not.throwError()
      expect(shareurl.setVerbosity).withArgs(999).to.throwError()
    })
    it('changes the verbosity level', () => {
      shareurl.setVerbosity(shareurl.DEBUG)
      expect(shareurl.getVerbosity()).to.equal(shareurl.DEBUG)
      shareurl.setVerbosity(shareurl.INFO)
      expect(shareurl.getVerbosity()).to.equal(shareurl.INFO)
      shareurl.setVerbosity(shareurl.WARN)
      expect(shareurl.getVerbosity()).to.equal(shareurl.WARN)
      shareurl.setVerbosity(shareurl.ERROR)
      expect(shareurl.getVerbosity()).to.equal(shareurl.ERROR)
    })
    it('changes the native verbosity level', () => {
      shareurl.setVerbosity(shareurl.INFO)
      expect(cordovaExecCallTo('setVerbosity')).to.be.ok()
      expect(cordovaExecCallTo('setVerbosity').args).to.eql([ shareurl.INFO ])
    })
  })

  describe('.numHandlers', () => {
    it('is a function', () => expect(shareurl.numHandlers).to.be.a('function'))
    it('returns the number of handlers', () => {
      expect(shareurl.numHandlers()).to.be.a('number')
      expect(shareurl.numHandlers()).to.equal(0)
    })
  })

  describe('.addHandler', () => {
    it('is a function', () => expect(shareurl.addHandler).to.be.a('function'))
    it('accepts only a function as argument', () => {
      expect(shareurl.addHandler).withArgs(() => {}).to.not.throwError()
      expect(shareurl.addHandler).withArgs('nope').to.throwError()
    })
    it('increases the number of handlers', () => {
      expect(shareurl.numHandlers()).to.equal(0)
      shareurl.addHandler(() => {})
      expect(shareurl.numHandlers()).to.equal(1)
    })
    it('refuses to add the same handler more than once', () => {
      const handler = () => {}
      expect(shareurl.numHandlers()).to.equal(0)
      shareurl.addHandler(handler)
      expect(shareurl.numHandlers()).to.equal(1)
      expect(shareurl.addHandler).withArgs(handler).to.throwError()
      expect(shareurl.numHandlers()).to.equal(1)
    })
  })

  describe('new file received', () => {
    var onNewFile
    var myHandlersArgs
    var myHandlers

    // test what happens for received files,
    // this requires to hack into some internal, to trigger a new file event
    beforeEach(() => {
      // There is the hack, we know that the onNewFile callback is expected by the
      // native side as argument 0 of the init function
      shareurl.init()
      onNewFile = cordovaExecCallTo('setHandler').successCallback
      // setup 2 handlers
      myHandlersArgs = [undefined, undefined]
      myHandlers = [
        function () { myHandlersArgs[0] = arguments },
        function () { myHandlersArgs[1] = arguments }
      ]
    })

    it('triggers all registered handlers', () => {
      // register handlers, check that they haven't been called yet
      myHandlers.forEach(shareurl.addHandler)
      myHandlersArgs.forEach((args) => {
        expect(args).to.not.be.ok()
      })

      // trigger a new file event and check that the handlers have been called
      var newFile = { test: 1 }
      onNewFile(newFile)
      myHandlersArgs.forEach((args) => {
        expect(args).to.be.ok()
        expect(args[0]).to.equal(newFile)
      })

      // do it again with another pseudo "file"
      var newFile2 = { test: 2 }
      onNewFile(newFile2)
      myHandlersArgs.forEach((args) => {
        expect(args[0]).to.equal(newFile2)
      })
    })

    it('triggers for handlers added after the new file is received', () => {
      var newFile = { test: 3 }
      onNewFile(newFile)
      myHandlers.forEach(shareurl.addHandler)
      myHandlersArgs.forEach((args) => {
        expect(args).to.be.ok()
        expect(args[0]).to.equal(newFile)
      })
    })
  })
})
